package automation_exercise;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class TestCase1 {

	
	@Test
	public void createCard() {
		//RestAssured.baseURI = "https://api.trello.com/";
		RestAssured.baseURI = "https://api.trello.com";
		RequestSpecification createCardRequest = RestAssured.given();
		createCardRequest.log().all();
		
		
		
		
		createCardRequest.header("Content-Type", "application/x-www-form-urlencoded");
		//createCardRequest.header("Content-Type", "multipart/form-data");
		//createCardRequest.contentType("application/json");
		//createCardRequest.header("Accept", "application/json");

		createCardRequest.queryParam("key", "0571642aefef5fa1fa76530ce1ba4c85");
		createCardRequest.queryParam("token", "8eb76d9a9d02b8dd40c2f3e5df18556c831d4d1fadbe2c45f8310e6c93b5c548");
		createCardRequest.queryParam("idList", "0571642aefef5fa1fa76530c");
		createCardRequest.queryParam("name", "Automation_Excercise");
		createCardRequest.queryParam("desc", "added by RestAssured");
		Response createCardResponse = createCardRequest.post("/1/cards");
		System.out.println(createCardResponse.statusCode());
		ResponseBody createCardResponseBody = createCardResponse.getBody();
		System.out.println(" Response Body : "+createCardResponseBody );
		
		
	}

}